#!/bin/bash


# The following is just for diagnostics and can be removed later on.
lsusb

until [ -c /dev/ttyUSB3 ]
do

      echo "Waiting for ttyUSB3.. Trying to power on the modem"
     
      # Following http://www.yantrr.com/wiki/SW_startup_guide_for_VIBE
      if [ ! -d /sys/class/gpio/gpio46  ]; then
	  echo 46 > /sys/class/gpio/export
      fi
      if [ ! -d /sys/class/gpio/gpio45  ]; then
	  echo 45 > /sys/class/gpio/export
      fi
      if [ ! -d /sys/class/gpio/gpio44  ]; then
	  echo 44 > /sys/class/gpio/export
      fi

      /bin/echo "high" >> /sys/class/gpio/gpio46/direction
      sleep 2
      /bin/echo "high" >> /sys/class/gpio/gpio45/direction
      sleep 2
      /bin/echo "high" >> /sys/class/gpio/gpio44/direction

      # Power on the modem as per
      # http://www.yantrr.com/wiki/ZTE_modem
      sleep 4
      /bin/echo "low" > /sys/class/gpio/gpio45/direction
      sleep 1
      /bin/echo "high" > /sys/class/gpio/gpio45/direction

done

# Soft reset the device
/bin/echo "low" > /sys/class/gpio/gpio44/direction
sleep 1
/bin/echo "high" > /sys/class/gpio/gpio44/direction

echo "Soft Resetting the Modem"
sleep 5

# The following is just for diagnostics and can be removed later on.
lsusb

./wvdial_auto_reconnect.sh &

# Add your application start code from this point onwards

while :
do
	echo "Running..."
	sleep 60
done
